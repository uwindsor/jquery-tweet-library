$(document).ready(function(){
  $('.twitter-feed').tweet({
        modpath: '/sites/all/libraries/jquery.tweet/index.js.php',
        count: 5,
        loading_text: 'loading twitter feed...',
        template: "{avatar} {text} {time} {reply_action} {retweet_action}",
        avatar_size: 0,
        page: 1,
        username: 'uwindsor'
        /* etc... */
    });
});